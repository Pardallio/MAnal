#ifndef __ACAO__
#define __ACAO__

double velocidade(double dt, double xfinal, double xinicial);
double lagrangeano(double xf, double xi, double v);
double acao(double tf, double ti, double xf, double R);
double acao2(double tf, double ti, double xf,double xi, double R);

#endif