#include <cstdio>
#include <cmath>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>

using namespace std;

double velocidade(double dt, double xfinal, double xinicial){
	double vinst;
	vinst=(xfinal-xinicial)/dt;
return vinst;
}

void aleatorios(double& r1,int& r2){
r1 =((double)rand()/(double)RAND_MAX)*2-1; //random between -1 and 1
r2 =(rand() % 2)*2 -1;                     //-1 or 1
}

double lagrangeano(double xf, double xi, double v){
	//double g, m; 
	//double l;
	//double x;
	//m=1;
	//g=-10;
	//x=(xf+xi)/2;
	//l=0.5*m*v*v+m*g*x;		//well defined Lagrangian
	double g;
    double l;
    g=-10;
	double x;
	x=(xf+xi)/2;
	l=sqrt((1+v*v)/(2*g*x));//x->t,y->x,y'->v
return l;
}
double lagrangeano2(double xf, double xi, double v){ 
	double l;
	double x;
	x=(xf+xi)/2;
	l=v*v-x*x;	
}
double lagrangeano3(double xf, double xi, double v){ 
	double g;
        double l;
	double x;
	x=(xf+xi)/2;
	l=sqrt((1+v*v)/(2*g*x));//x->t,y->x,y'->v	
}

/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*----------- The function acao was only a trial and doesn't satisfy the asked criteria.   -------*/
/*-----------------------------  Better Version is acao2 bellow  ---------------------------------*/
/*------------------------------------------------------------------------------------------------*/

double acao(double tf, double ti, double xf, double R){
	ofstream trajetoria;
	trajetoria.open ("trajetoria.txt"); //opens file where a trajectory will be made

	srand(time(NULL));
	double intervalot, dt,A,A1,A2;	//intervalot: total time interval of simulation
	int N;							//
	double r1;
	int r2;
	double R_salto=0.01*R; 
	double Lmuda1;
	double Lmuda2;
	//R is the radius to change the trajectory 

	vector <double> xizes;
	vector <double> ves;
	vector <double> Lbom;
	
	N=1e2;
	intervalot=tf-ti;
	//calculates the time step in which the Lagrangian is assumed constant 
	//(may change in future iterations of program) 
	dt=intervalot/N;

	cout << "tf- "<<tf<< "ti- "<<ti<< "xf- "<<xf;

	//creates an initial configuration of N positions in function of time x(t)
	for (int i = 0; i < N; ++i){
		xizes.push_back((double)i/N);
		//cout << "x("<< i <<")="<<xizes[i]<<";\n";
	}

	//approximates instantaneous velocities between the N positions, e.g. N-1 velocities v(t)
	for (int i = 0; i < (N-1); ++i){
		ves.push_back(velocidade(dt,xizes[i+1],xizes[i]));
		//cout << "v("<< i <<")="<<ves[i]<<";\n";
	}

	//calculates the Lagrangian for N-1 middle positions correspondent to the N-1 velocities L(t)
	for (int i = 0; i < (N-1); ++i){
		Lbom.push_back(lagrangeano(xizes[i+1],xizes[i],ves[i]));
		//cout << "L("<< i <<")="<<Lbom[i]*dt<<";\n";
	}

	//calculates: total action
	for (int i = 0; i < (N-1); ++i){
		A=A+Lbom[i]*dt;
	}
	A=A;
	A2=A+1;
	A1=A;

	//recalculates the Lagrangian for ith, (i-1)th time interval 
	//and with previous. Accepts if it is minimized 
	double xtemp; 			//temporary
	double Atemp; 			//temporary action
	double parar=1e-5;		//to stop the action
	int tit=0;
	int i;
	while(tit<1e+6){//converges for 1e+6 tit
		i=0;
		for (i = 1; i < (N-1); ++i){
			xtemp=xizes[i];
			Lmuda1=Lbom[i];
			Lmuda2=Lbom[i+1];

				aleatorios(r1, r2);
				//xizes[i]=xizes[i]+r1*R;//random walks
				xizes[i]=xizes[i]+r2*R_salto; //bouncy bouncy: takes ~60% of time relative to option above
				Lmuda1=lagrangeano(xizes[i],xizes[i-1],velocidade(dt,xizes[i],xizes[i-1]));
				Lmuda2=lagrangeano(xizes[i+1],xizes[i],velocidade(dt,xizes[i+1],xizes[i]));

				if ((Lmuda1+Lmuda2)<(Lbom[i]+Lbom[i-1])){
					Lbom[i-1]=Lmuda1;
					Lbom[i]=Lmuda2;
					//cout<<xizes [i]<<"__"<<xtemp<<endl;
				}
				else{
					xizes[i]=xtemp;
				}
		}

		A2=A1;
		for (int i = 0; i < (N-1); ++i){
			A1=A1+Lbom[i];
		}
		A1=A1*dt;
		tit=tit+1;
		//cout << "Minimizou \n"<< A1<< endl;
	}

	for (int i = 0; i < N; ++i){
		trajetoria << i*dt<<"  " <<xizes[i]<<"\n";
	}

	trajetoria.close();
	cout << "Fim!!\n"<< A1<< endl;
	return 0;
}

/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/

double acao2(double tf, double ti, double xf,double xi, double R){
	ofstream trajetoria;
	trajetoria.open ("trajetoria2.txt"); //opens file where a trajectory will be made

	srand(time(NULL));
	double intervalot,dt;			//intervalot: total time interval of simulation given by user, dt: time-step (determined) 
	double A,A1,A2;						//actions used throughout the program	
	int N=1e2;								//number of points considered in the trajectory
	double r1;								//no longer used (random real in radius around position)
	int r2;										//can take values -1 or 1 randomly
	double R_salto=0.01*R;		//auxiliary value (may become dynamic in future iterations)
	double Lmuda1, Lmuda2;		//auxiliary Lagrangian values (considered next to a certain position x[i])
	double xtemp; 						//saves the value of a position, temporarily
	double Atemp; 						//saves the value of a position, temporarily
	int i;										//iterator variable
	int count=0;							//counts number of tries for minimization before moving to next point
														//R is the radius to change the trajectory 

	vector <double> xizes;		//positions of particle
	vector <double> ves;			//velocities of particle
	vector <double> Lbom;			//free fall Lagrangian
	vector <double> Lbom2;		//to test a different Lagrangian
	

	intervalot=tf-ti;					//calculates the time step in which the Lagrangian is assumed constant 
														//(may change in future iterations of program) 
	dt=intervalot/N;

	//cout << "tf- "<<tf<< "ti- "<<ti<< "xf- "<<xf;

	//creates an initial configuration of N positions in function of time x(t)
	for (int i = 0; i < N; ++i){
		xizes.push_back((double)xi+(xf-xi)*i/N);
		//cout << "x("<< i <<")="<<xizes[i]<<";\n";
	}

	//approximates instantaneous velocities between the N positions, e.g. N-1 velocities v(t)
	for (int i = 0; i < (N-1); ++i){
		ves.push_back(velocidade(dt,xizes[i+1],xizes[i]));
		//cout << "v("<< i <<")="<<ves[i]<<";\n";
	}

	//calculates the Lagrangian for N-1 middle positions correspondent to the N-1 velocities L(t)
	for (int i = 0; i < (N-1); ++i){
		Lbom.push_back(lagrangeano(xizes[i+1],xizes[i],ves[i]));
		//cout << "L("<< i <<")="<<Lbom[i]*dt<<";\n";
	}
	for (int i = 0; i < (N-1); ++i){
		Lbom2.push_back(lagrangeano2(xizes[i+1],xizes[i],ves[i]));
		//cout << "L("<< i <<")="<<Lbom[i]*dt<<";\n";
	}


	//calculates: total action
	for (int i = 0; i < (N-1); ++i){
		A=A+Lbom[i]*dt;
		//AA=AA+Lbom2[i]*dt;
	}
	A=A;
	A2=A;
	A1=A;
	//cout << "\nInicio\n"<< AA<< endl;

	//recalculates the Lagrangian for ith, (i-1)th time interval 
	//and with previous. Accepts if it is minimized 
	while(1){
		i=0;
		for (i = 1; i < (N-1); ++i){
			xtemp=xizes[i];
			Lmuda1=Lbom[i];
			Lmuda2=Lbom[i+1];
			count=0;
			while(1){
				aleatorios(r1, r2);
				xizes[i]=xizes[i]+r2*R_salto; //bouncy bouncy: takes ~60% of time relative to previous option (random in an interval)
				Lmuda1=lagrangeano(xizes[i],xizes[i-1],velocidade(dt,xizes[i],xizes[i-1]));
				Lmuda2=lagrangeano(xizes[i+1],xizes[i],velocidade(dt,xizes[i+1],xizes[i]));

				if ((Lmuda1+Lmuda2)<(Lbom[i]+Lbom[i-1])){//if minimization succeeds accept new configuration
					Lbom[i-1]=Lmuda1;
					Lbom[i]=Lmuda2;
					break;
				}
				else{
					xizes[i]=xtemp;
					count++;
				}
				if(count>10) break;
			}
		}

		for (int i = 0; i < (N-1); ++i){
			A1=A1+Lbom[i];
		}
		A1=A1*dt;
		if ((A2-A1)<1e-6){						//iterates until difference in Actions calculated is 1e-6 
			cout << "\nbatata\n"<< A2-A1<< endl;
			break;
		}
		else{
			A2=A1;
		}
	}

for (int i = 0; i < N; ++i){
		trajetoria << i*dt<<"  " <<xizes[i]<<"\n";
	}

trajetoria.close();
	cout << "\nFim!!\n"<< A1<< endl;
return 0;
}

