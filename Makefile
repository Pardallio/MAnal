CC=g++

test: Action.o Trajectory.o test.o
	$(CC) -o $@ test.o Action.o Trajectory.o
	./$@

test.o:
	$(CC) -c test.cpp

Action.o:
	$(CC) -c Action.cpp

Trajectory.o:
	$(CC) -c Trajectory.cpp

clean:
	rm -f test *.o
