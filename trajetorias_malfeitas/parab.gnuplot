set terminal svg enhanced size 790,400 font "Verdana,14"
set output "AA.svg"
#set parametric
#set trange[0:pi]

set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 
set grid back ls 12  xtics ytics mxtics

set yrange[-5:40]

#queda de um grave
g=9.8;
ti=0;
tf=5;
xi=0;
xf=10;
b=(xf-xi)/(tf-ti)+g/2*(tf+ti);
a=xi-b*ti+g/2*ti**2;
f(x)=a+b*x-g/2*x**2

tf2=2.5;
b2=(xf-xi)/(tf2-ti)+g/2*(tf2+ti);
a2=xi-b2*ti+g/2*ti**2;
f2(x)=a2+b2*x-g/2*x**2

tf3=4.25;
b3=(xf-xi)/(tf3-ti)+g/2*(tf3+ti);
a3=xi-b3*ti+g/2*ti**2;
f3(x)=a3+b3*x-g/2*x**2


set dummy x


# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000080' lt 1 lw 1.5 pt 7 ps 0.3
set style line 2 lc rgb '#13C6CE' lt 1 lw 1.5 pt 7 ps 0.3
set style line 3 lc rgb '#399CA0' lt 1 lw 1.5 pt 7 ps 0.3
set style line 4 lc rgb '#4C8689' lt 1 lw 1.5 pt 7 ps 0.3


set style line 4 lc rgb '#00FF7F' lt 1 lw 0.5 pt 6 ps 2
set style line 10 lc rgb '#13CEC6' lt 1 lw 0.5 pt 6
set style line 9 lc rgb '#00E5DC' lt 1 lw 0.5 pt 6
#set style line 3 lc rgb '#00DCE5' lt 1 lw 0.5 pt 6 
#set style line 4 lc rgb '#13C6CE' lt 1 lw 0.5 pt 6 
set style line 5 lc rgb '#26B1B7' lt 1 lw 0.5 pt 6 
set style line 6 lc rgb '#399CA0' lt 1 lw 0.5 pt 6 
set style line 7 lc rgb '#4C8689' lt 1 lw 0.5 pt 6 
set style line 8 lc rgb '#5F7172' lt 1 lw 0.5 pt 6 


set border back

#Legenda
set key above
set key maxrows 1
set key maxcols 4
set key font ",12"
set key opaque
set key title 'Queda de um Grave' box
set key width -4
#set key at 1.825,42.2

set ylabel "x(m)"
set xlabel "t(s)"

#set ticslevel 0
#set view 65,300

#parabola
plot f(x) ls 4 notitle,\
     f2(x) ls 4 notitle,\
     f3(x) ls 4 notitle,\
     "parab1.txt" u 1:2 ls 1 title "(Δt=5s)",\
     "parab2.txt" u 1:2 ls 2 title "(Δt=2.5s)" ,\
     "parab3.txt" u 1:2 ls 3 title "(Δt=4.25s)",\
     "parab4.txt" u 1:2 ls 3 title "(Δt=2.5s),m<0"
     



