set terminal svg enhanced size 720,400 font "Verdana,14"
set output "AA.svg"

set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1 #dash type 3 
set grid back ls 12  ztics #mztics xtics ytics mxtics

set border 127

set ticslevel 0
set view 65,300

set xrange[0:33*1e3]

set xlabel "Nº de I-Iterações"
set xlabel offset 0,-1,0

set xtics 1e4
set mxtics 3

set format x "%.0sx10^{%S}"

set xtics scale 0.5

set  ylabel "t(s)"
set ytics offset 0,-0.2,0

set zlabel "x(m)"
set ztics 42

set title "Evolução da Trajectória ao Longo das I-Iterações" 
set title box
set nokey

splot "3dparabola.txt"  lc rgb "#000080"  pt 2 ps 0.35 t "Trajéctoria"  #,f(x,y) lw 0.5
#plot "trajetoria2.txt"