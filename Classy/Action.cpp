#include "Action.h"
#include "Trajectory.h"
#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include <functional>
#include <fstream>

Action::Action(Trajectory tr,double Rad,function<double(double,double,double)> lagr)
{
  srand(time(NULL));
  this->SetX_t(tr.GetVec());
  this->DefT(tr.GetTime(0),tr.GetTime(tr.GetSize()-1));
  nx_t.resize(this->GetSize());
  l_gran=lagr;
  flag_lag=true;
  CalcFill();
  radi=Rad;
  //cout <<val<<endl;
}


double Action::Lagrangean(int stiw)
{
  double x=GetXm(stiw);   //mean position on the ith interval
  double v=GetVel(stiw);  //mean velocity on the ith interval
  double t=GetTm(stiw);   //mean time on the ith interval
  
  return l_gran(x,v,t);
}

double Action::DLagrangean()
{  
  return 0  ;
}

void Action::DefLag(function<double(double,double,double)> lagr)
{
  flag_lag=true;
  l_gran=lagr;
}

void Action::Random(double& r1,int& r2){

r1 =((double)rand()/(double)RAND_MAX)*2-1; //random between -1 and 1
r2 =(rand() % 2)*2 -1;                    //-1 or 1

}

void Action::CalcFill()
{
  if(flag_lag)
    {
  int it=0;
  double tmp=0;
  double act=0;
  while(it<this->GetSize()-1)
    {
      tmp=Lagrangean(it);
      //     cout<<it<<endl;

      miniL.push_back(tmp);
      val=tmp+val;
      it=it+1;
    }
  val=val*(GetTime(1)-GetTime(0));
    }
}


double Action::Calc(bool flag_dt)
{
  int it=0;
  double tmp=0;
  double act=0;
  while(it<this->GetSize()-1)
    {
      tmp=Lagrangean(it);
      act=tmp+act;
      it=it+1;
    }
  return (flag_dt)? act*(GetTime(1)-GetTime(0)) : act;
}

double Action::Minimize(bool flag_bounce)
{
  if(flag_lag)
    {
      ofstream trajetoria;
      trajetoria.open ("trajetoria.txt"); //opens file where a trajectory will be made

      //recalculates the Lagrangian for ith, (i-1)th time interval 
      //and with previous. Accepts if it is minimized 
      double xtemp; 			//temporary
      double Lmuda1;        //auxiliary Lagrangian values 
      double Lmuda2;        //(considered next to a certain position x[i])

      double A,A1,A2;     //action stroing variables used throughout the program	

      int itr=0;
      int iti=0;
      int it;
      int count=0;

      A2=Calc(true);
  
      double r1;
      int r2;
      while(1)
	{
	  it=0;
	  for (it = 1; it < (GetSize()-1); ++it)
	    {
	      xtemp=GetXi(it);
	      count=0;
	      while(count<100)
		{
		  Random(r1, r2);
		  if(flag_bounce)
		    Defx_t(GetXi(it)+r2*radi,it);//random walks
		  else
		    Defx_t(GetXi(it)+r1*radi,it);//bouncy bouncy
     
		  Lmuda1=Lagrangean(it-1); //Lagrangean on the left interval of the ith point 
		  Lmuda2=Lagrangean(it);   //Lagrangean on the right interval of the ith point
		  //		  cout<<endl;
		  //if minimization succeeds accept new configuration
		  if ((Lmuda1+Lmuda2)<(miniL[it]+miniL[it-1]))
		    {
		      miniL[it-1]=Lmuda1;
		      miniL[it]=Lmuda2;     
		      break;
		    }
		  else
		    Defx_t(xtemp,it);
		  iti=iti+1;
		  count=count+1;
		}
	      //  cout<<itr<<endl;
	    }
	  //cout <<itr<<endl;
	  A1=Calc(true);
	  // cout<<A1<<endl;
	  if (abs((A2-A1))<1e-6&&itr>200)
	    {	    //iterates until difference between Actions calculated is 1e-6 
	      cout << "\nbatata\n"<< A2-A1<< endl;
	      break;
	    }
	  else
	    A2=A1;

	  itr=itr+1;
	}

      for (it = 0; it < GetSize(); ++it){
	trajetoria << GetTime(it)<<"  " <<GetXi(it)<<"\n";
      }
  
      trajetoria.close();
      cout << "\nFim!!\n"<< A1<< endl;
    }
}

//makes one iteration for one point
//1-> randomly selects the new position
//2-> calculates the action the for th2 2 adjacent intervals
//3-> returns true if action is minimized
bool Action::PIterate(const int& pi,const double& oa,bool debug)
{
  if(pi>0&&pi<GetSize())
    {
      double oldx=GetXi(pi);
      double r1=0;
      int r2=0;

      Random(r1,r2);       //Now we have a new configuration!
      if(GetXi(pi)+r1*radi>0)
	Defx_t(GetXi(pi)+r1*radi,pi);  //Let's compute the new integral
      
      double naction=0;
      naction=Lagrangean(pi-1)+Lagrangean(pi);

      if(debug&&pi==1)
	{
	  cout<<setprecision(6)<<"pi   ="<<pi<<endl
	      <<"Old x="<<oldx<<" Oac= "<<oa<<endl
	      <<"New x="<<GetXi(pi)<<" Newac= "<<naction<<endl;
	}
      if(naction<oa)
	{
	  //xcout<<"ola\n";
	  return false;
	}
      else
	{
	  Defx_t(oldx,pi);
	  return true;
	}
    }
  else
    return true;
}

void Action::IIterate()
{
  int it=1;
  int ti=0;
  int smax=GetSize();
  double olda=0;
  while(it<smax-1)
    {
      ti=0;
      olda=Lagrangean(it-1)+Lagrangean(it); //talvez n�o seja preciso calcular em todos
      while(PIterate(it,olda,false)&&ti<50)
	    ti=ti+1;
      // cout<<"\nNEW POINT\n"<<endl;
      it=it+1;
    }
}

//////////////////////////////////////////////
//////////////////////////////////////////////

void Action::Minimize2()
{
  double origa=Calc(true);
  IIterate();
  double atemp=Calc(true);
  int itcount=0;
  cout <<"inicio do fim\n"<<fabs(atemp-origa)<<endl;
  int flag=0;
  
  //ofstream acao;
  //acao.open ("acaoretardado.txt");

  while((itcount<1e3||fabs(atemp-origa)>radi*1e-7) && itcount<1e9)
    {
      if(itcount%100000==0)
	{
	  cout<<itcount<<" "<<fabs(atemp-origa)<<endl;
	}
      //in case of fail uncomment this
     //
      if(flag<3&&itcount%50==0)
	{
	  /*if(flag==0&&fabs(atemp-origa)<1e-3)
	    {
	      flag=flag+1;
	      DefRad(GetRad()*1e-2);
	    }
	    if(flag==1&&fabs(atemp-origa)<1e-6)
	    {
	      flag=flag+1;
	      DefRad(GetRad()*1e-2);
	    }//*/
	  //cout<<itcount<<" "<<fabs(atemp-origa)<<endl;
    //acao <<itcount<<"  " <<atemp<<"\n";
	  }
    
      
      origa=Calc(true);
      IIterate();
      atemp=Calc(true);
      //cout<<itcount<<endl;
      itcount=itcount+1;
    }

  cout<<origa<<"__::__"<<Calc(true)<<endl;

  ofstream trajetoria;
  trajetoria.open ("trajetoria.txt");
  
  for (int it = 0; it < GetSize(); ++it){
    trajetoria << GetTime(it)<<"  " <<GetXi(it)<<"\n";
  }
  //acao.close();
  trajetoria.close();
}

/* code for the 3D plot      
	if(itcount%(1000)==0)
	{
	cout<<itcount<<endl;
	for (int it = 0; it < GetSize(); ++it){
	trajetoria <<itcount<<"  "<< GetTime(it)<<"  " <<GetXi(it)<<"\n";
	}
	}
*/
