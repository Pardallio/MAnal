set terminal eps
set output "AA.eps"
#set parametric
#set trange[0:pi]


#queda de um grave
g=9.8;
ti=0;
tf=5;
xi=10;
xf=0;
b=(xf-xi)/(tf-ti)+g/2*(tf+ti);
a=xi-b*ti+g/2*ti**2;
f(x)=a+b*x-g/2*x**2

#acçao na queda de um grave
A(x)=-735.2
#A(x)=-1/(6*g)*((b-g*tf)**3-(b-g*ti)**3)-(g*a*(tf-ti)+0.5*g*b*(tf**2-ti**2)-1/6*g*g*(tf**3-ti**3))

#brachistochrone
k=sqrt(70)
#gy(t)=0.5*k**2*(t-sin(t))
#gx(t)=0.5*k**2*(1-cos(t))

#movimento oscilatorio
#h(x)=sin(x)
#fit para a fase
#h(x)=d*sin(a*x+b)+c
#a=1;
#b=0.1;
#c=0.1;
#d=10;
#fit h(x) "trajetoria.txt" using 1:2 via a,b,c,d


# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000080' lt 1 lw 1.5 pt 7 ps 0.2
set style line 2 lc rgb '#00FF7F' lt 1 lw 0.5 pt 6 ps 2
set style line 10 lc rgb '#13CEC6' lt 1 lw 0.5 pt 6
set style line 9 lc rgb '#00E5DC' lt 1 lw 0.5 pt 6
set style line 3 lc rgb '#00DCE5' lt 1 lw 0.5 pt 6 
set style line 4 lc rgb '#13C6CE' lt 1 lw 0.5 pt 6 
set style line 5 lc rgb '#26B1B7' lt 1 lw 0.5 pt 6 
set style line 6 lc rgb '#399CA0' lt 1 lw 0.5 pt 6 
set style line 7 lc rgb '#4C8689' lt 1 lw 0.5 pt 6 
set style line 8 lc rgb '#5F7172' lt 1 lw 0.5 pt 6 


#Legenda
set key top right Left title 'Variação do no de iterações para convergir' box

#set ticslevel 0
#set view 65,300


#parabola
#plot f(x) ls 2 title sprintf('x(t)=%1.2f+%1.2f*t-g/2*t^2',a,b), "trajetoria.txt" u 1:2 ls 1 title "Pontos simulados"

#Variação da acao
#set logscale x
#set logscale y
#plot "acao0.txt" u 1:($2+735) ls 10 title "r=1e-5",\
#     "acaoretardado.txt" u 1:($2+735) ls 9 title "r=1e-4",\
#     "acao2.txt" u 1:($2+735) ls 3 title "r=1e-3",\
#     "acao1.txt" u 1:($2+735) ls 4 title "r=1e-2",\
#     "acao3.txt" u 1:($2+735) ls 5 title "r=1e-1",\
#     "acao4.txt" u 1:($2+735) ls 6 title "r=1"

#no de iterações para convergir
plot "minimizar.txt" ls 2 title "wiiii"


#plot h(x) ls 2 title sprintf('B=%1.0f*sin(%1.2f*x+%1.2f)%1.0f',d,a,b,c), "trajetoria.txt" u 1:2 ls 1 title "Pontos simulados"
#plot 

#plot gx(t),gy(t) ls 2 title , "trajetoria.txt" u 1:2 ls 1 title "Pontos simulados"
#n_point, xi, xf, ti, tf:100,0,70,0,10
#plot "trajetoria2.txt"


