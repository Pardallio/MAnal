#include "Trajectory.h"
#include "Action.h"
#include <iostream>
#include <cmath>
#include <functional>
#include <ctime>
#include <string>
#include <fstream>

# define M_PI       3.14159265358979323846

using namespace std;

static double Lagrangean(double x, double v,double t)
{
/*return 0.5*v*v-9.8*x;*/
return 0.5*(sin(t)*sin(t)+(v-cos(t))*(1-cos(t)));
/*return sqrt((1+v*v)/(2*10*t));*/
/*return 0.5*v*v-0.5*x*x;*/
}

int main()
{
	const clock_t begin_time = clock();
    double passo=1e-1;
    int npassos;
    int count=10;
     //Trajectory t(100,0,sin(0.9*M_PI),0,0.9*M_PI);
    /*
    ofstream minimizar;
    minimizar.open ("minimizar.txt");
    Trajectory t(100,10,0,0,5);
    for (int i = 0; i < count; ++i)
    {
    	Action A(t,0.1+i*passo,Lagrangean);
    	A.Minimize2(npassos);
        minimizar << i*passo <<"  " << npassos <<"\n";
    }
    */

    //n_points, xi, xf, ti, tf
    Trajectory t(100,-M_PI,0,0,M_PI);
    Action A(t,1e-2,Lagrangean);
    A.Minimize2();



   
    //A.DefLag(Lagrangean);
    
    
    std::cout << float( clock () - begin_time ) /  CLOCKS_PER_SEC << endl;
    return 0;
}
