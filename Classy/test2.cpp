#include "Trajectory.h"
#include "Action.h"
#include <iostream>
#include <cmath>
#include <functional>

using namespace std;

static double Lagrangean(double x, double v,double t)
{
  return 0.5*v*v-10*x;//sqrt((1+v*v)/t);
}

int main()
{
  //           n_points, xi, xf, ti, tf
  Trajectory t(101,1,2,1,5);

  
  Action A(t,1e-4,Lagrangean);
  //A.DefLag(Lagrangean);
    int it=0;
  
  while(it<1000)
    {
      A.IIterate();
      it=it+1;
      }

      A.Print_to_file();
  //A.Minimize(true);
  
  return 0;
}
