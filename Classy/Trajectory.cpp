#include "Trajectory.h"

using namespace std;


//Construtores
Trajectory::Trajectory()
{
  size=0;
  x_t.clear();
  times[0]=0;
  times[1]=0;
  flag_DefT=false;
}

//initialize with size
Trajectory::Trajectory(int sz)
{
  size=sz;
  x_t.resize(sz);
  times[0]=0;
  times[0]=1;  
  flag_DefT=true;  
}

//initialize with size and type
Trajectory::Trajectory(int sz,string name)
{
  size=sz;
  x_t.resize(sz);
  times[0]=0;
  times[0]=1;  
  type=name;
  flag_DefT=false;
}

//Initialize with initial an final points
Trajectory::Trajectory(int n, double xi, double xf,double ti,double tf)
{
  if(n>=0)
    {
      x_t.clear();
      size=n;
      flag_DefT=true;
      times[0]=ti;
      times[1]=tf;
      double x_pos=0;
      int it=0;
      while(it<n)
	{
	  x_pos=xi+(xf-xi)*it/(n-1);
	  x_t.push_back(x_pos);
	  it=it+1;
	}
    }
}

Trajectory::Trajectory(vector<double>  v,string name)
{
  size=v.size();
  x_t=v;
  type=name;
  flag_DefT=false;
}

Trajectory::Trajectory(vector<double>  v,double ti,double tf,string name)
{
  size=v.size();
  x_t=v;
  type=name;
  times[0]=ti;
  times[1]=tf;
  flag_DefT=true;
}


Trajectory::Trajectory(vector<double> v)
{
  size=v.size();
  x_t=v;
  flag_DefT=false;
}


Trajectory::Trajectory(vector<double> v,double ti,double tf)
{
  size=v.size();
  x_t=v;
  times[0]=ti;
  times[1]=tf;
  flag_DefT=true;
}

Trajectory::~Trajectory()
{
  x_t.clear();
}

//get the type!
string Trajectory::GetType()
{
  return type;
}

double Trajectory::GetTime(int i)
{
  if(i>size-1 ||!flag_DefT)
    cout<<"I do nothing";
  else
    return i*(times[1]-times[0])/(size-1)+times[0];
}

//Returns the mean velocity on the ith interval
double Trajectory::GetVel(int i)
{
  if(i>size-1|!flag_DefT)
    cout<<"I do nothing";
  else
    {
      double dx=( x_t[i+1]-x_t[i]);
      double dt=( GetTime(i+1)-GetTime(i));
      return dx/dt;
    }
}

double Trajectory::GetXm(int i)
{
  if(i>size-1|!flag_DefT)
    cout<<"I do nothing";
  else
    {
      double dx=( x_t[i+1]+x_t[i]);
      return dx/2;
    }
}


double Trajectory::GetTm(int i)
{
  if(i>size-1|!flag_DefT)
    cout<<"I do nothing";
  else
    {
      double dt=( GetTime(i+1)+GetTime(i));
      return dt/2;
    }
}

void Trajectory::PrintX_t(int i)
{
  if(i>=size)
    cout<<"I do nothing";
  else
    cout<<"( "<<x_t[i]<<", "<<GetTime(i)<<" )\n";
}

//Defining the trajectory
void Trajectory::Defx_t(double x,int i)
{
  if(i>size-1)
    {
      cout <<"I do nothing\n";    
    }
  else
    {
      x_t[i]=x;
    }
}

void Trajectory::DefT(double ti, double tf)
{
  times[0]=ti;
  times[1]=tf;
  flag_DefT=true;
}

void Trajectory::GetVec(vector <double>& out)
{
  out=x_t;
}

vector<double> Trajectory::GetVec()
{
  return x_t;
}
