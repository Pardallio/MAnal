#ifndef __TRAJ__
#define __TRAJ__

#include <vector>
#include <string>
#include <iostream>

using namespace std;

class Trajectory
{
 public:
  Trajectory();
  ~Trajectory();

  Trajectory(int);
  Trajectory(int,string);
  Trajectory(int,double,double,double,double);
  Trajectory(vector<double> ,double,double,string);
  Trajectory(vector<double> );
  Trajectory(vector<double> ,string);
  Trajectory(vector<double> ,double,double);
  
  string GetType();
  double GetVel(int);
  double GetXm(int);
  double GetTm(int);
  int GetSize(){return size;};

  double GetTime(int);
  double GetXi(int i){if(i<size&&i>=0) return x_t[i];};
  void PrintX_t(int );

  void SetX_t(vector <double> ne){x_t=ne;size=ne.size();};
  void SetSize(int sz){size=sz;};
  void Defx_t(double x,int i);
  void DefT(double,double);
  void GetVec(vector <double>&);
  vector<double> GetVec();

  
 private:
  int size;
  string type;
  double times[2]; //initial and final time values [0]-> ti, [1]->tf
  bool flag_DefT;
  //2-dim vector. Goes like this
  // x| 0| 0.3| 0.5| 0.7
  // ===================
  // t| 1|  2 |  3 |  4
  vector<double> x_t;
};

#endif
