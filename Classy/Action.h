#ifndef __ACTION__
#define __ACTION__

#include "Trajectory.h"
#include <string>
#include <functional>

using namespace std;

class Action: public Trajectory
{
 public:
  Action(){val=0;nx_t.clear();};
  Action(Trajectory,double,function<double(double,double,double)>);

  void   CalcFill();
  double Calc(bool);
  
  void   Random(double&, int&);
  double Minimize(bool);
  void Minimize2();
  void IIterate();
  bool PIterate(const int&,const double&,bool);
  void DefLag(function<double(double,double,double)>);

  void DefRad(double r){radi=r;};
  double GetRad(){return radi;};
  
  
 private:
  double Lagrangean(int);
  double DLagrangean();
  
  function<double(double,double,double)> l_gran;

  //  double lagra(double,double,double);

  bool flag_lag;
  string lag;
  double radi;
  double val;           //Action value
  vector<double> miniL; //Lagrangian value in each interval
  vector<double> nx_t;  //possible new trajectory vector 
};
#endif
